build:
	docker-compose build

createsuperuser:
	docker-compose exec backend python manage.py createsuperuser --username=admin --email=admin@example.com

makemigrations:
	docker-compose exec backend python manage.py makemigrations

migrate:
	docker-compose exec backend python manage.py migrate

add_data:
	docker-compose exec backend python manage.py add_data_to_db