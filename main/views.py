from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import JsonResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import View
from django.db.models import Count, Max, Q, Prefetch
from django.views.generic import ListView, UpdateView, DeleteView, CreateView, DetailView

from main.models import Employee


# @login_required
class EmployeeTreeView(View):

    def get(self, request):
        root_nodes = Employee.objects.filter(parent__isnull=True).annotate(
            children_count=Count('children'),
            max_depth=Max('level')
        )

        for node in root_nodes:
            children = Employee.objects.filter(parent=node)
            node.children.set(children)

        return render(request, 'employee_tree.html', {'root_nodes': root_nodes})


# @login_required
class EmployeeListView(ListView):
    context_object_name = "employee_list"
    queryset = Employee.objects.all().prefetch_related(
        Prefetch('parent', queryset=Employee.objects.all())
    )
    template_name = "employee_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        employee_list = context['employee_list']

        paginator = Paginator(employee_list, 20)
        page = self.request.GET.get('page')

        try:
            employees = paginator.page(page)
        except PageNotAnInteger:
            employees = paginator.page(1)
        except EmptyPage:
            employees = paginator.page(paginator.num_pages)

        start_index = (employees.number - 1) * paginator.per_page
        context['employee_list'] = employees
        context['start_index'] = start_index
        context['search_query'] = self.request.GET.get('search_query', '')
        return context


# @login_required
class EmployeeFilterView(View):
    def get(self, request):
        sort_by = self.request.GET.get('sort_by')
        search_query = self.request.GET.get('search_query')
        queryset = Employee.objects.all()
        if search_query:
            queryset = queryset.filter(
                Q(first_name__icontains=search_query) |
                Q(last_name__icontains=search_query) |
                Q(second_name__icontains=search_query) |
                Q(position__icontains=search_query) |
                Q(hire_date__icontains=search_query)
            )
        if sort_by:
            queryset = queryset.order_by(sort_by)
        data = [{'first_name': employee.first_name,
                 'id': employee.id,
                 'last_name': employee.last_name,
                 'second_name': employee.second_name,
                 'email': employee.email,
                 'position': employee.position,
                 'hire_date': employee.hire_date,
                 } for employee in queryset]
        return JsonResponse(data, safe=False)


class LoadChildNodesView(View):

    def get(self, request):
        parent_id = request.GET.get('parent_id')
        try:
            parent = Employee.objects.get(id=parent_id)
            children = parent.children.all()
            data = [{'id': child.id,
                     'first_name': child.first_name,
                     'last_name': child.last_name,
                     'position': child.position,
                     'hire_date': child.hire_date,
                     } for child in children]
            return JsonResponse(data, safe=False)
        except Employee.DoesNotExist:
            return JsonResponse({'error': 'Parent employee not found'}, status=404)  # noqa


class EmployeeUpdateView(UpdateView):
    model = Employee
    fields = ["first_name",
              "last_name",
              "position",
              "hire_date",
              "email",
              "parent"]
    template_name_suffix = "_update_form"
    success_url = reverse_lazy("employees")


class EmployeeDeleteView(DeleteView):
    model = Employee
    success_url = reverse_lazy("employees")


class EmployeeCreateView(CreateView):
    model = Employee
    fields = ["first_name",
              "last_name",
              "position",
              "hire_date",
              "email",
              "parent"]
    success_url = reverse_lazy("employees")
