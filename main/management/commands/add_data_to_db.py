from django.core.management.base import BaseCommand
from django_seed import Seed
from main.models import Employee


class Command(BaseCommand):
    help = 'Populates the database with test records'

    def handle(self, *args, **options):
        seeder = Seed.seeder()
        seeder.add_entity(Employee, 50000)
        inserted_pks = seeder.execute()
        self.stdout.write(self.style.SUCCESS('Database successfully populated'))
