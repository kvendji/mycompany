from django.urls import path
from django.views.generic import TemplateView, DetailView

from main.models import Employee
from main.views import EmployeeTreeView, EmployeeListView, EmployeeFilterView, LoadChildNodesView, EmployeeUpdateView, \
    EmployeeDeleteView, EmployeeCreateView

urlpatterns = [
    path('', TemplateView.as_view(template_name='index.html'), name='index'),
    path('employees/tree/', EmployeeTreeView.as_view(), name='employee_tree'),
    path('employees/', EmployeeListView.as_view(), name='employees'),
    path('employees/filter/', EmployeeFilterView.as_view(), name='employees_filter_sort'),
    path('employees/children/', LoadChildNodesView.as_view(), name='employee_children'),
    path('employees/edit/<int:pk>/', EmployeeUpdateView.as_view(), name='employee_edit'),
    path('employees/delete/<int:pk>/', EmployeeDeleteView.as_view(), name='employee_delete'),
    path('employees/details/<int:pk>/', DetailView.as_view(model=Employee), name='employee_details'),
    path('employees/create/', EmployeeCreateView.as_view(), name='employee_create'),
]
