### Project "Employee Management Information System"

This project is designed for managing employees within an organization. The application provides the ability to view, add, edit, and delete employee information, as well as manage the hierarchy of employees in a tree-like structure.

### Technologies

This project is built using the following technologies:

- **Python **: Programming language used for server-side logic and database management.
- **Django**: High-level Python web framework for rapid development of web applications.
- **Django MPTT**: Django extension for working with tree-like data structures.
- **HTML, CSS, JavaScript**: Frontend technologies for creating the user interface.
- **Docker**: Platform for automating the deployment and management of applications in containers.

### Deployment Commands

To deploy the project, use the following commands:

```bash
# Build Docker image
docker-compose build

# Create Superuser:
docker-compose exec backend python manage.py createsuperuser --username=admin --email=admin@example.com

# Make Migrations:
docker-compose exec backend python manage.py makemigrations

# Apply Migrations:
docker-compose exec backend python manage.py migrate

# Add Data to Database:
docker-compose exec backend python manage.py add_data_to_db
